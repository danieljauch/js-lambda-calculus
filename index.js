// λx.x
const lIdentity = ifSame => ifSame;

// λt.λf.t
const lTrue = ifTrue => ifFalse => ifTrue;

// λt.λf.f
const lFalse = ifTrue => ifFalse => ifFalse;

// λc.λt.λf.c(t f)
const lIf = cond => ifTrue => ifFalse => cond(ifTrue, ifFalse);

// λc.λt.λf.c(f t)
const lNot = cond => ifFalse => ifTrue => cond(ifFalse, ifTrue);

// λc.λd.λt.λf.c(d(t f) f)
const lAnd = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, lIf(cond2, ifTrue, ifFalse), ifFalse);

// λc.λd.λt.λf.c(t d(t f))
const lOr = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, ifTrue, lIf(cond2, ifTrue, ifFalse));

// λc.λd.λt.λf.c(d(f t) t)
const lNotAnd = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, lIf(cond2, ifFalse, ifTrue), ifTrue);

// λc.λd.λt.λf.c(f d(f t))
const lNotOr = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, ifFalse, lIf(cond2, ifFalse, ifTrue));

// λc.λd.λt.λf.c(d(f t) d(t f))
const lExclusiveOr = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, lNot(cond2, ifTrue, ifFalse), lIf(cond2, ifTrue, ifFalse));

// λc.λd.λt.λf.c(d(t f) d(f t))
const lExclusiveNor = cond1 => cond2 => ifTrue => ifFalse =>
  lIf(cond1, cond2(ifTrue, ifFalse), lNot(cond2, ifTrue, ifFalse));
